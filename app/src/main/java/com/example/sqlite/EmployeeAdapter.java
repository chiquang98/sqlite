package com.example.sqlite;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sqlite.model.Department;
import com.example.sqlite.model.Employee;

import java.util.ArrayList;
import java.util.List;

public class EmployeeAdapter extends RecyclerView.Adapter<EmployeeAdapter.ViewHolder> {
    List<Employee> employeeList =new ArrayList<>();
    List<Department> departmentList = new ArrayList<>();
    Context mContext;

    public EmployeeAdapter(Context context,List<Employee> employeeList,List<Department> departmentList) {
        this.employeeList = employeeList;
        this.departmentList = departmentList;
        this.mContext = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater =LayoutInflater.from(parent.getContext());
        View view =inflater.inflate(R.layout.employee_item,parent,false);
        return new ViewHolder(view);
    }

    public String findNameDepartById(int id){
        for(Department department:departmentList){
            if(department.getId()==id){
                return department.getName();
            }
        }
        return "null";
    }
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.department.setText(findNameDepartById(employeeList.get(position).getDid()));
        holder.gender.setText(employeeList.get(position).getGender());
        holder.name.setText(employeeList.get(position).getName());
    }
    @Override
    public int getItemCount() {
        return employeeList.size();
    }
    class ViewHolder extends RecyclerView.ViewHolder{
        TextView name,gender,department;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.tvNameEmployee);
            gender = itemView.findViewById(R.id.tvGender);
            department = itemView.findViewById(R.id.tvDepartment);
        }
    }
}
