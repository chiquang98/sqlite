package com.example.sqlite.model;

public class Employee {
    private int id;
    private int did;
    private String name;
    private String gender;

    public Employee() {
    }

    public Employee(int id, int did, String name, String gender) {
        this.id = id;
        this.did = did;
        this.name = name;
        this.gender = gender;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDid() {
        return did;
    }

    public void setDid(int did) {
        this.did = did;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
