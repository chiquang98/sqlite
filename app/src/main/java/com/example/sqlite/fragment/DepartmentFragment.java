package com.example.sqlite.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.example.sqlite.R;
import com.example.sqlite.SQLITE.SQLiteHelper;
import com.example.sqlite.model.Department;
import com.example.sqlite.viewmodel.DepartmentViewModel;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class DepartmentFragment extends Fragment implements View.OnClickListener {
    SQLiteHelper sqLiteHelper;
    EditText edtName;
    Button btnAdd;
    DepartmentViewModel departmentViewModel;

    List<Department> listDepartment;
    public DepartmentFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        return inflater.inflate(R.layout.fragment_department, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initComponent(view);
    }

    private void initComponent(View view) {
        edtName = view.findViewById(R.id.edtNameDepart);
        btnAdd = view.findViewById(R.id.btnAdd);
        btnAdd.setOnClickListener(this);


    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        sqLiteHelper = new SQLiteHelper(getActivity());
        listDepartment  = sqLiteHelper.getAllDepartments();
        departmentViewModel = new ViewModelProvider(getActivity()).get(DepartmentViewModel.class);
        departmentViewModel.init(listDepartment);
        departmentViewModel.getDepartments().observe(getViewLifecycleOwner(), new Observer<List<Department>>() {
            @Override
            public void onChanged(List<Department> list) {
//                    for(Department d:list){
//                        System.out.println(d.getName());
//                    }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnAdd:
                String name = edtName.getText().toString();
                Department department = new Department();
                department.setName(name);
                long addId = sqLiteHelper.addDepartment(department);
                if (addId >= 0) {
                    Toast.makeText(getContext(), "Added " + addId, Toast.LENGTH_SHORT).show();
                    department.setId((int) addId);
                    departmentViewModel.addDepartment(department);

                } else {
                    Toast.makeText(getContext(), "Add failed", Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                break;
        }
    }
}
