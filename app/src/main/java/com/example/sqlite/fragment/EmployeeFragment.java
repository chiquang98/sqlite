package com.example.sqlite.fragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.sqlite.EmployeeAdapter;
import com.example.sqlite.R;
import com.example.sqlite.SQLITE.SQLiteHelper;
import com.example.sqlite.model.Department;
import com.example.sqlite.model.Employee;
import com.example.sqlite.viewmodel.DepartmentViewModel;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class EmployeeFragment extends Fragment implements View.OnClickListener, AdapterView.OnItemSelectedListener {
    SQLiteHelper sqLiteHelper;
    Spinner spinner;
    Button btnAdd,btnGetAll;
    EditText edtName,edtGender;
    RecyclerView recyclerView;
    int dIdSelected;
    DepartmentViewModel departmentViewModel;
    public EmployeeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        sqLiteHelper = new SQLiteHelper(getActivity());
        return inflater.inflate(R.layout.fragment_employee, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initComponent(view);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);



    }
    private int dID = 0;
    private void initComponent(View view) {
        spinner = view.findViewById(R.id.spinner);
        departmentViewModel = new ViewModelProvider(getActivity()).get(DepartmentViewModel.class);
        departmentViewModel.getDepartments().observe(getViewLifecycleOwner(), new Observer<List<Department>>() {
            @Override
            public void onChanged(final List<Department> list) {
                List<String> listNameDepart = new ArrayList<>();
                for(Department department:list){
                    listNameDepart.add(department.getName());
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_spinner_item,listNameDepart);
                adapter.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
                spinner.setAdapter(adapter);
                spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        dID = list.get(position).getId();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

            }
        });

        btnAdd = view.findViewById(R.id.btnAddEmploy);
        btnGetAll = view.findViewById(R.id.btnGetAll);
        edtGender = view.findViewById(R.id.edtGender);
        edtName = view.findViewById(R.id.edtNameEmployee);
        btnGetAll.setOnClickListener(this);
        btnAdd.setOnClickListener(this);
        recyclerView = view.findViewById(R.id.recycleView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager
                .VERTICAL, false));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnAddEmploy:
                System.out.println("INNN");
                Employee employee = new Employee();
                employee.setDid(dID);
                employee.setGender(edtGender.getText().toString());
                employee.setName(edtName.getText().toString());
                long addId = sqLiteHelper.insertEmployee(employee);
                if (addId >= 0) {
                    Toast.makeText(getContext(), "Added " + addId, Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(getContext(), "Add failed", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.btnGetAll:
                final List<Employee> employeeList = sqLiteHelper.getAllEmployee();
                departmentViewModel.getDepartments().observe(getViewLifecycleOwner(), new Observer<List<Department>>() {
                    @Override
                    public void onChanged(List<Department> list) {
                        EmployeeAdapter employeeAdapter = new EmployeeAdapter(getContext(),employeeList,list);
                        recyclerView.setAdapter(employeeAdapter);
                    }
                });
                break;
            default:
                break;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        // An item was selected. You can retrieve the selected item using
        // parent.getItemAtPosition(pos)
        if(parent.getSelectedItem()!=null){
            dIdSelected = Integer.parseInt(parent.getSelectedItem().toString());
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
