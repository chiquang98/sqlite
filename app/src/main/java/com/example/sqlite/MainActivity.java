package com.example.sqlite;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.viewpager.widget.ViewPager;

import com.example.sqlite.SQLITE.SQLiteHelper;
import com.example.sqlite.fragment.DepartmentFragment;
import com.example.sqlite.fragment.EmployeeFragment;
import com.example.sqlite.model.Department;
import com.example.sqlite.viewmodel.DepartmentViewModel;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    ViewPager pager;
    TabLayout tabs;
//    DepartmentViewModel departmentViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initTabLayout();
//        departmentViewModel = new ViewModelProvider(this).get(DepartmentViewModel.class);
//        departmentViewModel.init(new ArrayList<Department>());


    }

    private void initTabLayout() {
        TabLayout tabs = (TabLayout) findViewById(R.id.tabs);
        pager = findViewById(R.id.viewpager);
        setUpViewPager();
        tabs.setupWithViewPager(pager);
    }

    private void setUpViewPager() {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager(), 0);
        adapter.addFragment(new DepartmentFragment(), "Department");
        adapter.addFragment(new EmployeeFragment(), "Employee");
        pager.setAdapter(adapter);
    }

}
