package com.example.sqlite.SQLITE;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import com.example.sqlite.model.Department;
import com.example.sqlite.model.Employee;

import java.util.ArrayList;
import java.util.List;

public class SQLiteHelper extends SQLiteOpenHelper {
    static String DATABASE_NAME = "AppDemo";
    static int VERSION = 1;

    public SQLiteHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // Create table department
        String createDepartmentSql =
                "CREATE TABLE department(" +
                        "id INTEGER PRIMARY KEY AUTOINCREMENT," +
                        "name TEXT)";
        db.execSQL(createDepartmentSql);

        // Create table employee
        String createEmployeeSql =
                "CREATE TABLE employee(" +
                        "id INTEGER PRIMARY KEY AUTOINCREMENT," +
                        "did INTEGER," +
                        "name TEXT," +
                        "gender TEXT," +
                        "FOREIGN KEY (did) REFERENCES department(id))";
        db.execSQL(createEmployeeSql);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public long addDepartment(Department department) {
        ContentValues values = new ContentValues();
        values.put("name", department.getName());
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        return sqLiteDatabase.insert("department", null, values);
    }
    public List<Department> getAllDepartments() {
        List<Department> departments = new ArrayList<>();
        Cursor cursor = getReadableDatabase()
                .query("department", null, null,
                        null, null, null, null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                int id = cursor.getInt(cursor.getColumnIndex("id"));
                String name = cursor.getString(cursor.getColumnIndex("name"));
                Department department = new Department(id, name);
                departments.add(department);
            }
            cursor.close();
        }
        return departments;
    }
    public long insertEmployee(Employee employee) {
        ContentValues values = new ContentValues();
        values.put("did", employee.getDid());
        values.put("name", employee.getName());
        values.put("gender", employee.getGender());
        return getWritableDatabase().insert("employee", null, values);
    }
    public List<Employee> getAllEmployee() {
        List<Employee> employees = new ArrayList<>();
        Cursor cursor = getReadableDatabase()
                .query("employee", null, null,
                        null, null, null, null);
        if (cursor!= null){
            while (cursor.moveToNext()){
                int id = cursor.getInt(cursor.getColumnIndex("id"));
                int did = cursor.getInt(cursor.getColumnIndex("did"));
                String name = cursor.getString(cursor.getColumnIndex("name"));
                String gender = cursor.getString(cursor.getColumnIndex("gender"));
                Employee employee = new Employee(id, did, name, gender);
                employees.add(employee);
            }
            cursor.close();
        }
        return employees;
    }
}
