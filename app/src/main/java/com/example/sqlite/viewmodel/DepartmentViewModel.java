package com.example.sqlite.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.sqlite.model.Department;

import java.util.List;

public class DepartmentViewModel extends ViewModel {

    private MutableLiveData<List<Department>> listDepartment;

    public LiveData<List<Department>> getDepartments(){
        if(listDepartment==null){
            listDepartment = new MutableLiveData<>();
            loadDepartment();
        }
        return listDepartment;
    }

    private void loadDepartment() {
    }
    public void init(List<Department> list){
        if(listDepartment==null){
            listDepartment = new MutableLiveData<>();
            listDepartment.postValue(list);
        }
    }
    public void addDepartment(Department department){
        if(listDepartment!=null){
            List<Department> currentDepartment = listDepartment.getValue();
            currentDepartment.add(department);
            listDepartment.postValue(currentDepartment);
        }
    }
}
